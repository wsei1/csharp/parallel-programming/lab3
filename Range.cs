﻿namespace LAB3
{
	public class Range
	{
		public Range(decimal minValue, decimal maxValue)
		{
			MinValue = minValue;
			MaxValue = maxValue;
		}

		public decimal MinValue { get; set; }
		public decimal MaxValue { get; set; }
	}
}
