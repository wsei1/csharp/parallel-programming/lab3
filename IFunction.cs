﻿namespace LAB3
{
	public interface IFunction
	{
		int Id { get; }
		string Name { get; }
		decimal GetY(decimal x);
	}
}
