﻿namespace LAB3
{
	public class Function1 : IFunction
	{
		public int Id => 1;

		public string Name => "y = 2x + 2x^2";

		public decimal GetY(decimal x)
		{
			return 2 * x + 2 * (x * x);
		}
	}
}
