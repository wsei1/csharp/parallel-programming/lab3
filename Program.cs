﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LAB3
{
	class Program
	{
		static void Main(string[] args)
		{
			ISystemThreading systemThreading;
			List<IFunction> Functions = new List<IFunction>
			{
				new Function1(),
				new Function2(),
				new Function3()
			};

			Console.WriteLine("Function list: ");
			foreach (IFunction functionData in Functions)
			{
				Console.WriteLine($"Id: {functionData.Id}, Name: {functionData.Name}");
			}

			var functionChoice = int.Parse(Console.ReadLine());

			if (!Functions.Any(x => x.Id == functionChoice))
			{
				Console.WriteLine("You picked wrong number.");
			}
			else
			{
				var choosedFunction = Functions.FirstOrDefault(func => func.Id == functionChoice);

				Console.WriteLine("Threading systems: ");
				Console.WriteLine("1. BackgroundWorker");
				Console.WriteLine("2. TPL");

				var threadingChoice = int.Parse(Console.ReadLine());
				switch (threadingChoice)
				{
					case 1:
						systemThreading = new Worker(choosedFunction);
						systemThreading.Run();
						break;
					case 2:
						systemThreading = new TPL(choosedFunction);
						systemThreading.Run();
						break;
					default:
						Console.WriteLine("You picked wrong number.");
						break;
				}
			}

			Console.ReadKey();
		}
	}
}
