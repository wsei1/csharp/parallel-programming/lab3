﻿namespace LAB3
{
	public class Function2 : IFunction
	{
		public int Id => 2;

		public string Name => "y = 2x^2";

		public decimal GetY(decimal x)
		{
			return 2 * (x * x);
		}
	}
}
