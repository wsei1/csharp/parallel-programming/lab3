﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LAB3
{
	public class Worker : ISystemThreading
	{
		List<Range> ranges = new List<Range>();
		List<NameBackgroundWorker> bw = new List<NameBackgroundWorker>();
		private IFunction Function;
		public decimal total = 100 * 3;
		public int iteractions;
		public Worker(IFunction function)
		{
			Function = function;
		}

		public void Run()
		{
			ranges.Add(new Range(-10, 10));
			ranges.Add(new Range(-5, 20));
			ranges.Add(new Range(-5, 0));

			bw.Add(new NameBackgroundWorker("BackgroundWorker1"));
			bw.Add(new NameBackgroundWorker("BackgroundWorker2"));
			bw.Add(new NameBackgroundWorker("BackgroundWorker3"));

			Parallel.For(0, bw.Count, (i) =>
			{
				bw.ElementAt(i).WorkerReportsProgress = true;
				bw.ElementAt(i).WorkerSupportsCancellation = true;
				bw.ElementAt(i).DoWork += DoWork;
				bw.ElementAt(i).ProgressChanged += ProgressChanged;
				bw.ElementAt(i).RunWorkerCompleted += WorkCompleted;
				bw.ElementAt(i).RunWorkerAsync(ranges.ElementAt(i));
			});

			Console.WriteLine("Press C to terminate");
			while (bw.Any(x => x.IsBusy))
			{
				if (Console.ReadKey(true).KeyChar == 'c')
				{
					Parallel.ForEach(bw, worker =>
					{
						worker.CancelAsync();
					});
				}
			}
		}

		private void DoWork(object sender, DoWorkEventArgs e)
		{
			var parameters = e.Argument as Range;
			var worker = sender as NameBackgroundWorker;
			var result = 0m;
			var minValue = parameters.MinValue;
			var maxValue = parameters.MaxValue;
			decimal width = (maxValue - minValue) / 100;
			int counter = 0;

			for (decimal x = minValue; x + width <= maxValue; x += width)
			{
				if (worker.CancellationPending)
				{
					e.Cancel = true;
					break;
				}

				var a = Function.GetY(x);
				var b = Function.GetY(x + width);
				var field = ((a + b) * width) / 2;
				result += field;
				if (counter % 10 == 0)
				{
					worker.ReportProgress(counter);
				}
				counter++;
				Thread.Sleep(100);
			}
			e.Result = result;
		}

		private void ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			var backgroundWorker = sender as NameBackgroundWorker;
			Console.WriteLine($"Progress of {backgroundWorker.Name}: {e.ProgressPercentage}%");
		}

		private void WorkCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			var backgroundWorker = sender as NameBackgroundWorker;
			Console.WriteLine($"Work in {backgroundWorker.Name} completed. Result = {e.Result}");
		}
	}
}
