﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LAB3
{
	public class TPL : ISystemThreading
	{
		IFunction Function;
		List<Task> Tasks = new List<Task>();

		public TPL(IFunction function)
		{
			Function = function;
		}

		public async void Run()
		{
			CancellationTokenSource tokenSource = new CancellationTokenSource();
			CancellationToken token = tokenSource.Token;

			Task firstTask = new Task(() => CalculateIntegral(Function, new Range(-10, 10), "First task"), token);
			Task secondTask = new Task(() => CalculateIntegral(Function, new Range(-5, 20), "Second task"), token);
			Task thirdTask = new Task(() => CalculateIntegral(Function, new Range(-5, 0), "Third task"), token);

			Tasks.Add(firstTask);
			Tasks.Add(secondTask);
			Tasks.Add(thirdTask);

			Console.WriteLine("Start task execution.");
			
			firstTask.Start();
			secondTask.Start();
			thirdTask.Start();

			await Task.WhenAll(firstTask, secondTask, thirdTask);

			Console.WriteLine("Finished.");
		}

		private void CalculateIntegral(IFunction function, Range range, string name)
		{
			var width = 0.2m;
			var result = 0m;
			var minValue = range.MinValue;
			var maxValue = range.MaxValue;
			var counter = 0;
			var amountOfIterations = (Math.Abs(minValue) + Math.Abs(maxValue)) / width;
			var tenPer = (amountOfIterations * 10) / 100;
			var progressValue = 0;
			decimal currentPercentValue = 0;

			for (decimal x = minValue; x + width <= maxValue; x += width)
			{
				var a = function.GetY(x);
				var b = function.GetY(x + width);
				var field = ((a + b) * width) / 2;
				result += field;
				if (counter == Math.Ceiling(currentPercentValue))
				{
					ReportProgress(progressValue, name);
					progressValue += 10;
					currentPercentValue += tenPer;
				}
				counter++;
				Thread.Sleep(100);
			}
			Console.WriteLine($"\nTaskName: {name}.\nFunction {function.Name} executed for range {range.MinValue} to {range.MaxValue}. Result: {result}.\n");
		}

		private void ReportProgress(int value, string name)
		{
			Console.WriteLine($"TaskName: {name}. Progress is {value}%.");
		}
	}
}
