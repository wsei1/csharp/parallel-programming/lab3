﻿using System.ComponentModel;
using System.Threading;

namespace LAB3
{
	class NameBackgroundWorker : BackgroundWorker
	{
        public NameBackgroundWorker(string name)
        {
            Name = name;
        }

        public string Name { get; private set; }

        protected override void OnDoWork(DoWorkEventArgs e)
        {
            if (Thread.CurrentThread.Name == null)
                Thread.CurrentThread.Name = Name;

            base.OnDoWork(e);
        }
    }
}
